import 'dart:io';

import 'package:auth_jmt_login_firebas/views/widgets/botaoCustomizado.dart';
import 'package:auth_jmt_login_firebas/views/widgets/input_customizado.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:brasil_fields/brasil_fields.dart';
import 'package:form_field_validator/form_field_validator.dart';

class NovoAnuncio extends StatefulWidget {
  const NovoAnuncio({Key? key}) : super(key: key);

  @override
  _NovoAnuncioState createState() => _NovoAnuncioState();
}

class _NovoAnuncioState extends State<NovoAnuncio> {
  //final requiredValidator = RequiredValidator(errorText: 'OBRIGATÓRIO SELECIONAR UM ESTADO');

  List<File> _listaImagens = [];

  final _formKey = GlobalKey<FormState>();

  List<DropdownMenuItem<String>> _listaitemDropEstados = [];
  List<DropdownMenuItem<String>> _listaitemDropCategorias = [];

  String? _itemSelecionadoEstado;
  String? _itemSelecionadoCategoria;

  _selecionarImagemGaleria() async {
    final picker = ImagePicker();
    final imagemSelecionada =
        await picker.getImage(source: ImageSource.gallery);
    if (imagemSelecionada != null) {
      setState(() {
        _listaImagens.add(File(imagemSelecionada.path));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _carregarItensDropdown();
  }

  _carregarItensDropdown() {
    _listaitemDropCategorias.add(DropdownMenuItem(
      child: Text("Automovel"),
      value: "Auto",
    ));
    _listaitemDropCategorias.add(DropdownMenuItem(
      child: Text("Imovel"),
      value: "casa",
    ));
    _listaitemDropCategorias.add(DropdownMenuItem(
      child: Text("Eletronicos"),
      value: "Eletro",
    ));
    _listaitemDropCategorias.add(DropdownMenuItem(
      child: Text("Moda"),
      value: "Modas",
    ));
    _listaitemDropCategorias.add(DropdownMenuItem(
      child: Text("Esportes"),
      value: "Sport",
    ));

    // _listaitemDropEstados.add(DropdownMenuItem(child: Text("Cuiabá"), value: "Cuiabá")
    // );
    // _listaitemDropEstados.add(DropdownMenuItem(child: Text("São Paulo"), value: "Sao paulo")
    // );
    // _listaitemDropEstados.add(DropdownMenuItem(child: Text("Minas Gerais"), value: "Minas Gerais")
    // );

    for (var estado in Estados.listaEstadosSigla) {
      _listaitemDropEstados.add(DropdownMenuItem(
        child: Text(estado),
        value: estado,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Novo Anuncio"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: 100,
                  child: ListView.builder(
                      itemCount: _listaImagens.length + 1,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, indice) {
                        if (indice == _listaImagens.length) {
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: GestureDetector(
                              onTap: () {
                                _selecionarImagemGaleria();
                              },
                              child: CircleAvatar(
                                backgroundColor: Colors.grey[400],
                                radius: 50,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.add_a_photo,
                                      size: 40,
                                      color: Colors.grey[100],
                                    ),
                                    Text(
                                      "Adicionar",
                                      style: TextStyle(color: Colors.grey[100]),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        }
                        if (_listaImagens.length > 0) {
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 8),
                            child: GestureDetector(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (context) => Dialog(
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Image.file(_listaImagens[indice]),
                                              TextButton(
                                                onPressed: () {
                                                  setState(() {
                                                    _listaImagens
                                                        .removeAt(indice);
                                                    Navigator.of(context).pop();
                                                  });
                                                },
                                                child: Text(
                                                  "Excluir",
                                                  style: TextStyle(
                                                      color: Colors.red),
                                                ),
                                              )
                                            ],
                                          ),
                                        ));
                              },
                              child: CircleAvatar(
                                radius: 50,
                                backgroundImage:
                                    FileImage(_listaImagens[indice]),
                                child: Container(
                                  color: Color.fromRGBO(255, 255, 255, 0.4),
                                  alignment: Alignment.center,
                                  child: Icon(Icons.delete, color: Colors.red),
                                ),
                              ),
                            ),
                          );
                        }
                        return Container();
                      }),
                ),
                FormField<List>(
                  initialValue: _listaImagens,
                  validator: (imagens) {
                    if (imagens!.length == 0) {
                      return "Necessário inserir uma imagem";
                    }
                    return null;
                  },
                  builder: (state) {
                    return Column(
                      children: [
                        Container(),
                        if (state.hasError)
                          Container(
                            child: Text(
                              "[${state.errorText}]",
                              style: TextStyle(fontSize: 14, color: Colors.red),
                            ),
                          )
                      ],
                    );
                  },
                ),
                // meus Dropdown
                Row(
                  children: [
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.all(8),
                      child: DropdownButtonFormField(
                        value: _itemSelecionadoEstado,
                        hint: Text("Estados"),
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.7), fontSize: 20),
                        items: _listaitemDropEstados,
                        validator: (valor) {
                          if (valor == null)
                            return RequiredValidator(
                                    errorText: 'selecione um estado')
                                .errorText;
                        },
                        onChanged: (valor) {
                          setState(() {
                            _itemSelecionadoEstado = valor as String?;
                          });
                        },

                        // RequiredValidator(errorText: 'Escolha um estado valido'),
                        // validator: (valor){
                        //   if(valor == null) return "Digite algum estado";
                        // },
                      ),
                    )),
                    Expanded(
                        child: Padding(
                      padding: EdgeInsets.all(8),
                      child: DropdownButtonFormField(
                        value: _itemSelecionadoCategoria,
                        hint: Text("Categoria"),
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.7), fontSize: 20),
                        items: _listaitemDropCategorias,
                        validator: (valor) {
                          if (valor == null)
                            return RequiredValidator(
                                    errorText: 'selecione um estado')
                                .errorText;
                        },
                        onChanged: (valor) {
                          setState(() {
                            _itemSelecionadoCategoria = valor as String?;
                          });
                        },

                        // RequiredValidator(errorText: 'Escolha um estado valido'),
                        // validator: (valor){
                        //   if(valor == null) return "Digite algum estado";
                        // },
                      ),
                    )),
                  ],
                ),
                //Caixa de textos e botoes
                Padding(
                  padding: const EdgeInsets.only(bottom: 15, top: 15),
                  child: InputCustomizado(
                    hint: "Titulo",
                    maxlines: 1,
                    inputFormatters: [],
                    validator: (valor) {
                      if (valor!.isEmpty)
                        return "digite um titulo";
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: InputCustomizado(
                    hint: "Preço",
                    maxlines: 1,
                    type: TextInputType.number,
                    inputFormatters: [
                      // ignore: deprecated_member_use
                      WhitelistingTextInputFormatter.digitsOnly,
                      RealInputFormatter(centavos: true)
                    ],
                    validator: (valor) {
                      if (valor!.isEmpty)
                        return RequiredValidator(
                                errorText: 'Digite um preço')
                            .errorText;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: InputCustomizado(
                    hint: "Telefone",
                    type: TextInputType.phone,
                    inputFormatters: [
                      // ignore: deprecated_member_use
                      WhitelistingTextInputFormatter.digitsOnly,
                      TelefoneInputFormatter()
                    ],
                    validator: (valor) {
                      if (valor!.isEmpty)
                        return RequiredValidator(
                                errorText: 'Digite um telefone')
                            .errorText;
                    },
                    maxlines: 1,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: InputCustomizado(
                    hint: "Descricao",
                    maxlines: 3,
                    inputFormatters: [],
                    validator: (valor) {
                      if (valor!.isEmpty)
                        return RequiredValidator(
                                errorText: 'Digite uma descricao')
                            .errorText;
                    },
                  ),
                ),

                BotaoCustomizado(
                    texto: "Cadastrar Anuncio",
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {

                        _formKey.currentState!.save();
                      }

                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
