import 'package:auth_jmt_login_firebas/models/usuario.dart';
import 'package:auth_jmt_login_firebas/views/widgets/botaoCustomizado.dart';
import 'package:auth_jmt_login_firebas/views/widgets/input_customizado.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController _controllerEmail =
      TextEditingController(text: "washiner@gmail.com");
  TextEditingController _controllerSenha =
      TextEditingController(text: "123456789");

  bool _cadastrar = false;
  String _menssagemErro = "";
  String _textoBotao = "Entrar";

  _cadastrarUsuario(Usuario usuario) {
    FirebaseAuth auth = FirebaseAuth.instance;
    auth.createUserWithEmailAndPassword(
      email: usuario.email,
      password: usuario.senha,
    ).then((firebaseUser) {
      //direcionar para tela principal
      Navigator.pushReplacementNamed(context, "/");
    });
  }

  _logarUsuario(Usuario usuario) {

    FirebaseAuth auth = FirebaseAuth.instance;
    auth.signInWithEmailAndPassword(
        email: usuario.email,
        password: usuario.senha
    ).then((FirebaseUser) {
      //direcionar para tela principal
      Navigator.pushReplacementNamed(context, "/");

    });
  }
  _validarCampos() {
    String email = _controllerEmail.text;
    String senha = _controllerSenha.text;

    if (email.isNotEmpty && email.contains("@")) {
      if (senha.isNotEmpty && senha.length > 6) {
        Usuario usuario = Usuario();
        usuario.email = email;
        usuario.senha = senha;

        if (_cadastrar) {
          _cadastrarUsuario(usuario);
        } else {
          _logarUsuario(usuario);
        }
      } else {
        setState(() {
          _menssagemErro = "Preencha a senha! digite mais de 6 caracter";
        });
      }
    } else {
      setState(() {
        _menssagemErro = "Preencha email válido";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Seja bem vindo !!!"),
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 32),
                  child: Image.asset(
                    "lib/assets/logo.jpg",
                    width: 200,
                    height: 180,
                  ),
                ),
                InputCustomizado(
                  controller: _controllerEmail,
                  hint: "E-mail",
                  autofocus: true,
                  type: TextInputType.emailAddress,
                  validator: (valor) {
                    RequiredValidator(errorText: 'emailll');
                  },
                ),
                SizedBox(height: 10),
                InputCustomizado(
                  controller: _controllerSenha,
                  hint: "Senha",
                  validator: (valor) {
                    RequiredValidator(errorText: 'senhaa');
                  },
                  //obscure: true,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("LOGAR"),
                    Switch(
                      value: _cadastrar,
                      onChanged: (bool valor) {
                        setState(() {
                          _cadastrar = valor;
                          _textoBotao = "Entrar";
                          if(_cadastrar){
                            _textoBotao = "Cadastrar";
                          }
                        });
                      },
                    ),
                    Text("CADASTRAR")
                  ],
                ),
                BotaoCustomizado(texto: _textoBotao,
                    onPressed: (){
                     _validarCampos();
                    }),
                Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text(
                    _menssagemErro,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
