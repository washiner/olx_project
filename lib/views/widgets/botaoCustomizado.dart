import 'dart:ui';

import 'package:flutter/material.dart';

class BotaoCustomizado extends StatelessWidget {
  final String texto;
  final Color corTexto;
  final VoidCallback onPressed;

  BotaoCustomizado(
      {required this.texto,
      this.corTexto = Colors.purple,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    final String texto;
    final Color corTexto;

    return ElevatedButton(
      child:
          Text("Meus Anúncios".toUpperCase(), style: TextStyle(fontSize: 14)),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(this.corTexto),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            //side: BorderSide(color: Colors.white),
          ),
        ),
      ),
      onPressed: this.onPressed,
    );
  }
}
